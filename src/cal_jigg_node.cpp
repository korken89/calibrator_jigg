/****************************************************************************
*
* Copyright (C) 2016 Emil Fresk.
* All rights reserved.
*
* This file is part of the ROS KFly Telemetry library.
*
* GNU Lesser General Public License Usage
* This file may be used under the terms of the GNU Lesser
* General Public License version 3.0 as published by the Free Software
* Foundation and appearing in the file LICENSE included in the
* packaging of this file.  Please review the following information to
* ensure the GNU Lesser General Public License version 3.0 requirements
* will be met: http://www.gnu.org/licenses/lgpl-3.0.html.
*
* If you have questions regarding the use of this file, please contact
* Emil Fresk at emil.fresk@gmail.com.
*
****************************************************************************/

#include <string>
#include <thread>
#include <chrono>

#include "SerialPipe/serialpipe.h"
#include "ros/ros.h"
#include "calibration_jigg_control/CalibrationStatus.h"

using namespace std::literals::chrono_literals;

const int steps_per_rotation = 3200;
const int revolutions_per_round = 4;


ros::Publisher pub;

void sample_acc(void)
{
  calibration_jigg_control::CalibrationStatus status;
  status.header.stamp = ros::Time::now();
  status.message.data = "acc start";
  pub.publish(status);

  std::this_thread::sleep_for(10s);

  status.header.stamp = ros::Time::now();
  status.message.data = "acc stop";
  pub.publish(status);

  std::this_thread::sleep_for(500ms);
}

void sample_gyro_start(void)
{
  std::this_thread::sleep_for(200ms);
  calibration_jigg_control::CalibrationStatus status;
  status.header.stamp = ros::Time::now();
  status.message.data = "gyro start";
  pub.publish(status);

  std::this_thread::sleep_for(1s);
}

void sample_gyro_stop(void)
{
  std::this_thread::sleep_for(1s);
  calibration_jigg_control::CalibrationStatus status;
  status.header.stamp = ros::Time::now();
  status.message.data = "gyro stop";
  pub.publish(status);

  std::this_thread::sleep_for(500ms);
}

std::string response;

bool send_command_wait_for_responses(SerialPipe::SerialBridge &sp,
                                     const std::string &cmd,
                                     const std::string &expected_response1,
                                     const std::string &expected_response2 = "")
{
  sp.serialTransmit(cmd + "\n");

  if (expected_response2.size() > 0)
    ROS_INFO_STREAM("Command '" + cmd + "' waiting for '" + expected_response1 + "' and then '" + expected_response2 + "'");
  else
    ROS_INFO_STREAM("Command '" + cmd + "' waiting for '" + expected_response1 + "'");

  while(response.size() == 0)
    std::this_thread::sleep_for(10ms);

  if (response == expected_response1)
  {
    response.clear();

    if (expected_response2.size() > 0)
    {
      while(response.size() == 0)
        std::this_thread::sleep_for(10ms);

      if (response == expected_response2)
      {
        //ROS_INFO("Command got expected");
        response.clear();
        std::this_thread::sleep_for(100ms);
        return true;
      }
      else
      {
        ROS_ERROR("Command got not expected");
        response.clear();
        std::this_thread::sleep_for(100ms);
        return false;
      }
    }

    //ROS_INFO("Command got expected");
    std::this_thread::sleep_for(100ms);
    return true;
  }
  else
  {
    ROS_ERROR("Command got not expected");
    response.clear();
    std::this_thread::sleep_for(100ms);
    return false;
  }
}


void move_servo(SerialPipe::SerialBridge &sp, int id, int pos)
{
  std::string cmd = "servo" + std::to_string(id) + " " + std::to_string(pos);
  send_command_wait_for_responses(sp, cmd, "ok");
  if (id == 1)
    std::this_thread::sleep_for(5s);
  else
    std::this_thread::sleep_for(2s);
  //send_command_wait_for_responses(sp, "servos_off", "ok");
}

enum class Servo1_Pos {
  UP_SIDE_DOWN = 103,
  UP_SIDE_DOWN_UP = 94,
  FRONT_DOWN = 113,
  FLAT = 128,
  FRONT_HALF_DOWN = (FRONT_DOWN + FLAT) / 2,
  FRONT_UP = 142,
  FRONT_HALF_UP = (FRONT_UP + FLAT) / 2
};

enum class Servo2_Pos {
  RIGHT_DOWN = 250,
  RIGHT_UP = 20,
  FLAT = 137,
  RIGHT_HALF_DOWN = (RIGHT_DOWN + FLAT) / 2,
  RIGHT_HALF_UP = (RIGHT_UP + FLAT) / 2,
  DOWN_FLAT = 230,
  DOWN_RIGHT = 120,
  DOWN_HALF_DOWN = 190//(DOWN_FLAT + DOWN_RIGHT) / 2

};

void move_servo(SerialPipe::SerialBridge &sp, Servo1_Pos pos)
{
  move_servo(sp, 1, static_cast<int>(pos));
}

void move_servo(SerialPipe::SerialBridge &sp, Servo2_Pos pos)
{
  move_servo(sp, 2, static_cast<int>(pos));
}

void rotate(SerialPipe::SerialBridge &sp, int pos)
{
  send_command_wait_for_responses(sp, "goto " + std::to_string(pos), "ok", "done");
}

void rotate_set(SerialPipe::SerialBridge &sp)
{
  sample_gyro_start();
  rotate(sp,  steps_per_rotation * revolutions_per_round );
  sample_gyro_stop();
  sample_gyro_start();
  rotate(sp, 0);
  sample_gyro_stop();
  sample_gyro_start();
  rotate(sp,  steps_per_rotation * revolutions_per_round );
  sample_gyro_stop();
  sample_gyro_start();
  rotate(sp, 0);
  sample_gyro_stop();
}

void rec_data(const std::vector<uint8_t> &data);

int main(int argc, char *argv[])
{
  /*
   * Initializing ROS
   */
  ROS_INFO("Initializing IMU Calibration Jigg Node...");
  ros::init(argc, argv, "calibration_jigg");

  ros::NodeHandle nh("~");
  ros::NodeHandle n;

  pub = n.advertise<calibration_jigg_control::CalibrationStatus>("calibration_info", 10);

  /* Get all system parameters. */
  std::string port;
  int baud;

  if (!nh.getParam("port", port))
  {
    ROS_ERROR("No port specified, aborting!");
    return -1;
  }
  ROS_INFO("Port:     %s", port.c_str());

  if (!nh.getParam("baudrate", baud))
  {
    ROS_ERROR("No baudrate specified, aborting!");
    return -1;
  }
  ROS_INFO("Baudrate: %d", baud);

  if (baud < 9600)
  {
    ROS_ERROR("The baudrate must be 9600 or more, aborting!");
    return -1;
  }

  SerialPipe::SerialBridge sp(port, baud, 100, true, "\r");

  /* Register a callback */
  sp.registerCallback(rec_data);

  /* Open the serial port */
  sp.openPort();

  if (sp.isOpen())
    ROS_INFO("Calibration Jigg port open.");
  else
  {
    ROS_INFO("Unable to open port, aborting.");
    return -1;
  }

  ROS_INFO("Sending settings...");
  send_command_wait_for_responses(sp, "acc 6000", "ok");
  send_command_wait_for_responses(sp, "speed 1000", "ok");


  ROS_INFO("Homing...");
  send_command_wait_for_responses(sp, "home 300", "ok", "done");

  ROS_INFO("\n\n****************************\nGyro round, flat-flat\n****************************\n\n");

  /*
   * 1/5
   */
  ROS_INFO("First round, flat, right-down");
  move_servo(sp, Servo1_Pos::FLAT);
  move_servo(sp, Servo2_Pos::RIGHT_DOWN);
  rotate_set(sp);
  ROS_INFO("Done");


  ROS_INFO("First round, flat, right-half-down");
  move_servo(sp, Servo2_Pos::RIGHT_HALF_DOWN);
  rotate_set(sp);
  ROS_INFO("Done");


  ROS_INFO("First round, flat, flat");
  move_servo(sp, Servo2_Pos::FLAT);
  rotate_set(sp);
  ROS_INFO("Done");


  ROS_INFO("First round, flat, right-half-down");
  move_servo(sp, Servo2_Pos::RIGHT_HALF_UP);
  rotate_set(sp);
  ROS_INFO("Done");


  ROS_INFO("First round, flat, right-down");
  move_servo(sp, Servo2_Pos::RIGHT_UP);
  rotate_set(sp);
  ROS_INFO("Done");



  /*
   * 2/5
   */
  ROS_INFO("Second round, front-half-down, right-down");
  move_servo(sp, Servo1_Pos::FRONT_HALF_DOWN);
  move_servo(sp, Servo2_Pos::RIGHT_DOWN);
  rotate_set(sp);
  ROS_INFO("Done");


  ROS_INFO("Second round, front-half-down, right-half-down");
  move_servo(sp, Servo2_Pos::RIGHT_HALF_DOWN);
  rotate_set(sp);
  ROS_INFO("Done");


  ROS_INFO("Second round, front-half-down, flat");
  move_servo(sp, Servo2_Pos::FLAT);
  rotate_set(sp);
  ROS_INFO("Done");


  ROS_INFO("Second round, front-half-down, right-half-up");
  move_servo(sp, Servo2_Pos::RIGHT_HALF_UP);
  rotate_set(sp);
  ROS_INFO("Done");


  ROS_INFO("Second round, front-half-down, right-up");
  move_servo(sp, Servo2_Pos::RIGHT_UP);
  rotate_set(sp);
  ROS_INFO("Done");

  /*
   * 3/5
   */
  ROS_INFO("Third round, front-down, flat");
  move_servo(sp, Servo1_Pos::FRONT_DOWN);
  move_servo(sp, Servo2_Pos::DOWN_FLAT);
  rotate_set(sp);
  ROS_INFO("Done");

  /*
   * 4/5
   */
  ROS_INFO("Fourth round, front-half-up, right-down");
  move_servo(sp, Servo1_Pos::FRONT_HALF_UP);
  move_servo(sp, Servo2_Pos::RIGHT_DOWN);
  rotate_set(sp);
  ROS_INFO("Done");


  ROS_INFO("Fourth round, front-half-up, right-half-down");
  move_servo(sp, Servo2_Pos::RIGHT_HALF_DOWN);
  rotate_set(sp);
  ROS_INFO("Done");


  ROS_INFO("Fourth round, front-half-up, flat");
  move_servo(sp, Servo2_Pos::FLAT);
  rotate_set(sp);
  ROS_INFO("Done");


  ROS_INFO("Fourth round, front-half-up, right-half-up");
  move_servo(sp, Servo2_Pos::RIGHT_HALF_UP);
  rotate_set(sp);
  ROS_INFO("Done");


  ROS_INFO("Fourth round, front-half-up, right-up");
  move_servo(sp, Servo2_Pos::RIGHT_UP);
  rotate_set(sp);
  ROS_INFO("Done");


  /*
   * 5/5
   */
  ROS_INFO("Fifth round, front-up, flat");
  move_servo(sp, Servo1_Pos::FRONT_UP);
  move_servo(sp, Servo2_Pos::DOWN_FLAT);
  rotate_set(sp);
  ROS_INFO("Done");


  ROS_INFO("\n\n****************************\nAcclerometer round, flat-flat\n****************************\n\n");

  rotate(sp, 350);
  move_servo(sp, Servo1_Pos::FLAT);
  move_servo(sp, Servo2_Pos::FLAT);

  float diff = static_cast<float>(Servo1_Pos::FRONT_UP) - static_cast<float>(Servo1_Pos::UP_SIDE_DOWN_UP);
  float start1 =  static_cast<float>(Servo1_Pos::FRONT_UP);
  float step1 = diff / 7;

  diff = static_cast<float>(Servo2_Pos::RIGHT_UP) - static_cast<float>(Servo2_Pos::RIGHT_DOWN);

  float start2 =  static_cast<float>(Servo2_Pos::RIGHT_UP);
  float step2 = diff / 4;

  for (int i = 0; i < 8; i++)
  {
    move_servo(sp, 1, static_cast<int>( start1 - step1 * i - 0.5 ));

    if (i != 0 && i != 4)
    {
      move_servo(sp, Servo2_Pos::RIGHT_DOWN);
      sample_acc();
      move_servo(sp, Servo2_Pos::RIGHT_HALF_DOWN);
      sample_acc();
      move_servo(sp, Servo2_Pos::FLAT);
      sample_acc();
      move_servo(sp, Servo2_Pos::RIGHT_HALF_UP);
      sample_acc();
      move_servo(sp, Servo2_Pos::RIGHT_UP);
      sample_acc();
    }
    else
      sample_acc();

  }


  ROS_INFO("\n\n****************************\nDone!\n****************************\n\n");

  ROS_INFO("Reseting servos");

  move_servo(sp, Servo1_Pos::FLAT);
  move_servo(sp, Servo2_Pos::FLAT);

  /* Let ROS run. */
  ros::spin();

  return 0;
}

std::string serial_str;
void rec_data(const std::vector<uint8_t> &data)
{
  for (uint8_t ch : data)
  {
    if (ch == '\n' || ch == '\r')
    {
      if (serial_str.size() > 0)
      {
        while (response.size() > 0)
          std::this_thread::sleep_for(10ms);

        response = serial_str;
        //ROS_INFO_STREAM("Received: " << serial_str);
        serial_str.clear();
      }
    }
    else
    {
      serial_str += static_cast<char>(ch);
    }
  }
}
